#!/bin/bash

repeticiones=4
echo "------NOMBRE DEL EQUIPO----"
uname -a
echo "\n---------INFO CPU----------"
cat /proc/cpuinfo
echo "\n---------INFO MEM----------"
cat /proc/meminfo

for app in "opm1" "opm2" "opm3"
do
	echo -e "\n\n......................................"
	echo -e "-------------------$app-----------------"
	echo -e "......................................\n"

	for proc in `seq 2 24`
	do
		echo -e  "\n\nAHORA CON $proc PROCESADORES"
		export OMP_NUM_THREADS=$proc
		for i in `seq $repeticiones`
		do
			./$app 15000 25000	| grep "Tiempo"
		done
	done
done
