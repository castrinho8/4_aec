// Programa que calcula una norma para cada una de las     |
// filas de una matriz bidimensional representada mediante |
// un formato de almacenamiento comprimido.                |
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <pmmintrin.h>


#define INDEX(i,j) [(i)*ncols+j]

void inicializacion(float **pvalores, float **pnorma, int nfilas, int ncols);

int main( int argc, char *argv[] ) {
	
	float *valores; 
   float *norma;
	float normafila;
	int nfilas, ncols;
	int i, j;
	struct timeval t0, t1, t;
	float x0, x1, x2, x3;
	float pssuma_arr[4];

	__m128 psfila1, psfila2, psfila3, psfila4;
	__m128 pssuma,resto;
	
	if (argc != 3) {
		printf ("Uso: norma nfilas ncols \n");
		exit(0);
	}
	nfilas = atoi(argv[1]);
	ncols = atoi(argv[2]);

	inicializacion(&valores, &norma, nfilas, ncols);
	assert (gettimeofday (&t0, NULL) == 0);

	for (i=0; i<nfilas - nfilas%4; i+=4)
	{
		pssuma = _mm_setzero_ps();
		x0=0; x1=0; x2=0; x3=0;
		
		for (j=0; j<ncols - ncols%4; j+=4) 
		{
			psfila1 = _mm_loadu_ps(&(valores INDEX(i,j))); 
			psfila2 = _mm_loadu_ps(&(valores INDEX(i+1,j)));
			psfila3 = _mm_loadu_ps(&(valores INDEX(i+2,j)));
			psfila4 = _mm_loadu_ps(&(valores INDEX(i+3,j)));
			
			psfila1 = _mm_mul_ps(psfila1,psfila1);
			psfila2 = _mm_mul_ps(psfila2,psfila2);
			psfila3 = _mm_mul_ps(psfila3,psfila3);
			psfila4 = _mm_mul_ps(psfila4,psfila4);
			
			psfila1 = _mm_hadd_ps(psfila1,psfila2);
			psfila3 = _mm_hadd_ps(psfila3,psfila4);
			
			psfila1 = _mm_hadd_ps(psfila1,psfila3);
			
			pssuma = _mm_add_ps(pssuma,psfila1); 
		}

		for(;j<ncols;j++)
		{
			x0 += (valores INDEX(i,j)) * (valores INDEX(i,j));
			x1 += (valores INDEX(i+1,j)) * (valores INDEX(i+1,j));
			x2 += (valores INDEX(i+2,j)) * (valores INDEX(i+2,j));
			x3 += (valores INDEX(i+3,j)) * (valores INDEX(i+3,j));
		}

		resto = _mm_set_ps(x3,x2,x1,x0);
		pssuma = _mm_add_ps(resto,pssuma);


		pssuma = _mm_sqrt_ps(pssuma);
		_mm_storeu_ps(norma + i,pssuma);
	}

	for(;i<nfilas;i++)
	{
		x0 = 0;
		pssuma = _mm_setzero_ps();
		
		for (j=0; j<ncols - ncols%4; j+=4) 
		{
			psfila1 = _mm_loadu_ps(&(valores INDEX(i,j)));  //cargar los datos en var
			psfila1 = _mm_mul_ps(psfila1,psfila1); //multiplicar por si mismo
			pssuma = _mm_add_ps(pssuma,psfila1); //suma suma+b
		}
		
		for(;j<ncols;j++)
			x0 += (valores INDEX(i,j))*(valores INDEX(i,j));

		_mm_storeu_ps(pssuma_arr,pssuma);
		norma[i] = sqrt(pssuma_arr[0] + pssuma_arr[1] + pssuma_arr[2] + pssuma_arr[3] + x0);

	}
	
	assert (gettimeofday (&t1, NULL) == 0);
	timersub(&t1, &t0, &t);

	 for(i=0;i<5;i++) printf("Norma[%d] = %.2f \n",i,norma[i]);
      for(i=nfilas-5;i<nfilas;i++) printf("Norma[%d] = %.2f \n",i,norma[i]);
	
	printf ("nfilas      = %ld\n", nfilas);
	printf ("ncols       = %ld\n", ncols);
	printf ("Tiempo      = %ld:%ld(seg:mseg)\n", t.tv_sec, t.tv_usec/1000);
	
	_mm_free(valores); 
	_mm_free(norma);
	return 0;
}


void inicializacion(float **pvalores, float **pnorma, int nfilas, int ncols)
{
	int i, j;


	if( ((*pvalores)=(float *) _mm_malloc(sizeof(float)*nfilas*ncols,16)) == NULL ) {
		printf ("error: memoria insuficiente para valores\n");
		exit(-1);
	}

	if( ((*pnorma)=(float *) _mm_malloc(sizeof(float)*nfilas,16)) == NULL ) {
		printf ("error: memoria insuficiente para norma\n");
		exit(-1);
	}

	for (i=0; i<nfilas; i++) {
		for (j=0; j<ncols; j++) {
			(*pvalores) INDEX(i,j) = (float) ( (float) random() / (float) RAND_MAX );
		}
	}
}

