#!/bin/bash

repeticiones=10
echo "------NOMBRE DEL EQUIPO----"
uname -a
echo "\n---------INFO CPU----------"
cat /proc/cpuinfo
echo "\n---------INFO MEM----------"
cat /proc/meminfo

for app in "p3c" "pSSEcvec" "manvec1" "manvec2" "manvec3"
do
	echo -e "\n\n......................................"
	echo -e "-------------------$app-----------------"
	echo -e "......................................\n"

	for j in 15000 20000 22500 25000
	do
		echo -e  "\n\nAHORA CON $j COLUMNAS"
		for i in `seq $repeticiones`
		do
			./$app 15000 $j	| grep "Tiempo"
		done
	done
done
