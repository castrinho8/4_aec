// Programa que calcula una norma para cada una de las     |
// filas de una matriz bidimensional representada mediante |
// un formato de almacenamiento comprimido.                |
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <pmmintrin.h>


#define INDEX(i,j) [(i)*ncols+j]

void inicializacion(float **pvalores, float **pnorma, float **pssuma_arr, int nfilas, int ncols);

int main( int argc, char *argv[] ) {
	
	float *valores,sumamod; 
   float *norma;
	float normafila;
	float *pssuma_arr; //NOVA
	int nfilas, ncols;
	int i, j=0;
	struct timeval t0, t1, t;
	
	__m128 psfila;
	__m128 pspotencia;
	__m128 pssuma;
	
	if (argc != 3) {
		printf ("Uso: norma nfilas ncols \n");
		exit(0);
	}
	nfilas = atoi(argv[1]);
	ncols = atoi(argv[2]);

	inicializacion(&valores, &norma,&pssuma_arr, nfilas, ncols);
	assert (gettimeofday (&t0, NULL) == 0);

	for (i=0; i<nfilas; i++) {
		sumamod = 0;
		pssuma = _mm_setzero_ps();
		
		for (j=0; j<ncols - ncols%4; j+=4) {
			psfila = _mm_loadu_ps(valores+i*ncols+j);  //cargar los datos en var
			pspotencia = _mm_mul_ps(psfila,psfila); //multiplicar por si mismo
			pssuma = _mm_add_ps(pssuma,pspotencia); //suma suma+b
		}
		
		for(;j<ncols;j++)
			sumamod += (valores INDEX(i,j))*(valores INDEX(i,j));
		
		_mm_store_ps(pssuma_arr,pssuma);
		norma[i] = sqrt(pssuma_arr[0] + pssuma_arr[1] + pssuma_arr[2] + pssuma_arr[3] + sumamod);
	}
	
	assert (gettimeofday (&t1, NULL) == 0);
	timersub(&t1, &t0, &t);

	 for(i=0;i<5;i++) printf("Norma[%d] = %.2f \n",i,norma[i]);
      for(i=nfilas-5;i<nfilas;i++) printf("Norma[%d] = %.2f \n",i,norma[i]);
	
	printf ("nfilas      = %ld\n", nfilas);
	printf ("ncols       = %ld\n", ncols);
	printf ("Tiempo      = %ld:%ld(seg:mseg)\n", t.tv_sec, t.tv_usec/1000);
	
	_mm_free(valores); 
	_mm_free(norma);
	_mm_free(pssuma_arr);
	return 0;
}


void inicializacion(float **pvalores, float **pnorma, float **pssuma_arr, int nfilas, int ncols)
{
	int i, j;


	if( ((*pvalores)=(float *) _mm_malloc(sizeof(float)*nfilas*ncols,16)) == NULL ) {
		printf ("error: memoria insuficiente para valores\n");
		exit(-1);
	}

	if( ((*pnorma)=(float *) _mm_malloc(sizeof(float)*nfilas,16)) == NULL ) {
		printf ("error: memoria insuficiente para norma\n");
		exit(-1);
	}
	
	if( ((*pssuma_arr)=(float *) _mm_malloc(sizeof(float)*4,16)) == NULL ) {
		printf ("error: memoria insuficiente para array de suma\n");
		exit(-1);
	}

	for (i=0; i<nfilas; i++) {
		for (j=0; j<ncols; j++) {
			(*pvalores) INDEX(i,j) = (float) ( (float) random() / (float) RAND_MAX );
		}
	}
}

