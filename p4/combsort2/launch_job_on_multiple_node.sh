# Number of MPI processes (= Number of SVG nodes)
MPIPROCESSES=$1
echo "Number of MPI processes:   "$MPIPROCESSES

# Number of OpenMP threads per MPI process (= Number of OpenMP threads per SVG node)
OMPTHREADS=$2
echo "Number of OpenMP threads:  "$OMPTHREADS

# Send job to queue...
qsub -cwd -l arch=amd,num_proc=$OMPTHREADS,s_rt=00:30:00,s_vmem=2G,h_fsize=1G -pe mpi_1p $MPIPROCESSES -N job-mpi$MPIPROCESSES-openmp$OMPTHREADS trabajo.sh

