#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <time.h>
#include "tree.c"

#define CALC_IT_PER_PROC(N,P,R) N/P + (R < N % P)


void
print_results(	int * array,
				long numberofitems)
{
	long i;
	for(i = 0; i < 5; i++)
		printf(" array[%li]: %i\n", i, array[i]);
	if(numberofitems > 5)
		for(i = numberofitems - 5;i < numberofitems;i++)
			printf(" array[%li]: %i\n", i, array[i]);
}

int
check_results(	int * array,
				long numberofitems)
{
	long i;
	for(i=1;i<numberofitems;i++)
		if(array[i] < array[i-1])
			return 0;
	return 1;
}

void
generate_data(	int ** data,
				long numberofitems)
{
	long i;
	*data = malloc(sizeof(int) * numberofitems);
	assert(*data);

	srand(time(NULL));

	for(i=0;i<numberofitems;i++)
		(*data)[i] = rand();
}

int omp_thread_count() {
    int n = 0;
    #pragma omp parallel reduction(+:n)
    n += 1;
    return n;
}

void
combsort(int a[], long nElements)
{
	short swapped = 0;
	long i, j, gap;
	int temp;

	gap = nElements;
	while (gap > 1 || swapped)
	{
		gap = gap * 10 / 13;
		if (gap == 9 || gap == 10) gap = 11;
		if (gap < 1) gap = 1;

		swapped = 0;
		for (i = 0, j = gap; j < nElements; i++, j++)
		{
			if (a[i] > a[j])
			{
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
				swapped = 1;
			}
		}
	}
}


void
omp_sort (int a[], long nElements)
{
	long  it_p_th, i, *mx_sta, *mx_end;
	int num_th, thread, * aux, proc, value;
	t_tree tree;

	num_th = omp_thread_count();

	mx_sta = malloc(sizeof(long) * num_th);
	mx_end = malloc(sizeof(long) * num_th);

	it_p_th = nElements / num_th + ((nElements % num_th) > 0);

	#pragma omp parallel for private(thread,i)
	for(i=0; i < nElements; i += it_p_th)
	{
		thread = omp_get_thread_num();

		mx_sta[thread] = i;
		mx_end[thread] = i + it_p_th;
		if(mx_end[thread] >= nElements)
			mx_end[thread] = nElements;

		combsort(a + mx_sta[thread], mx_end[thread] - mx_sta[thread]);

		#ifdef DEBUG
			printf("This is thread %i sorting (%i,%i) -> %i\n",
					thread, mx_sta[thread],mx_end[thread], mx_end[thread] - mx_sta[thread]);
		#endif

		#ifdef CHECKOMP
			int myrank;
			MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
			printf("OMP check. Proc. %2i - Thread %2i (%i-%i): %s\n",myrank, thread, mx_sta[thread], mx_end[thread],
					check_results(a + mx_sta[thread], mx_end[thread] - mx_sta[thread]) ? "Correct" : "Error");
		#endif
	}


	new_tree(&tree);
	i = 0;
	aux = malloc(sizeof(int) * nElements);

	for(proc=0; proc < num_th; proc++)
	{
		if(mx_sta[proc] < mx_end[proc])
		{
			insert_tree(&tree,
						a[mx_sta[proc]],
						proc);
			(mx_sta[proc])++;
		}
	}

	do
	{
		pop_smaller_tree(&tree, &value, &proc);
		aux[i++] = value;

		if(mx_sta[proc] < mx_end[proc])
		{
			insert_tree(&tree,
						a[mx_sta[proc]],
						proc);
			(mx_sta[proc])++;
		}
	}while(no_empty_tree(tree));

	#pragma omp parallel for
	for(i=0; i< nElements; i++)
		a[i] = aux[i];

	free(aux);
}


int
main(int argc, char **argv)
{

	int numprocs, myrank, i, proc, indiceroot, indiceres, *sendcounts, *displs, recvcount;
	int value, *subarray, *data;
	t_tree tree = NULL;
	double mytime;
	MPI_Status status;
	long numberofitems = -1;

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

	if(argc != 2 && !myrank)
	{
		printf("Usage: ./combsort <number of items>\n");
		return -1;
	}

	numberofitems = atol(argv[1]);

	if(!myrank){

		if(numberofitems <= 0){
			printf("The number of items must be a positive number.\n");
			return -1;
		}

		//Generate a random array of ints.
		generate_data(&data,numberofitems);

		mytime = MPI_Wtime();
	}


   // Fase 1: Comunicación
   // Scatter del array que se quiere ordenar
   // Def. procedimiento DistribucionArray()
    //CALCULAR PARAMETROS SCATTER
    if( !myrank ){

      sendcounts = malloc(sizeof(int) * numprocs);
      assert(sendcounts);

      displs = malloc(sizeof(int) * numprocs);
      assert(displs);

    	for(i=0; i < numprocs; i++)
    	{
    		sendcounts[i] = CALC_IT_PER_PROC(numberofitems, numprocs, i);
    		displs[i] = i ? displs[i-1] + sendcounts[i-1] : 0;
    	}
    }

    recvcount = CALC_IT_PER_PROC(numberofitems, numprocs, myrank);
    subarray = malloc(sizeof(int) * recvcount);
    assert(subarray);

    MPI_Scatterv( 	data,
        			sendcounts,
                    displs,
                    MPI_INT,
                    subarray,
                    recvcount,
                    MPI_INT,
                    0,
                    MPI_COMM_WORLD);

   // Fase 2: Computo
   // Ordenación secuencial local en cada proceso MPI
	omp_sort(subarray,(long) recvcount);


	#ifdef CHECKCHILD
	printf("MPI Check. Proceso %i : %s\n",myrank, check_results(subarray,
		(long) recvcount) ? "Correct" : "Incorrect");
	#endif

   // Fase 3: Comunicación
   // Ordenación global a partir de los arrays ordenados localmente
   // Def. procedimiento: OrdenamientoGlobal()
	//ORDENAR ENTRE PROC.
	if(myrank != 0) // Non pai.
	{
		//ENVIASE SECUENCIALMENTE TODOS OS DATOS.
		for(i=0;i<recvcount;i++)
		{
			#ifdef DEBUG
				printf("Proc. %i envia dato num. %i de %i : %i\n",myrank,i,recvcount,*(subarray +i));
			#endif
			MPI_Ssend(	subarray + i,
						1,
						MPI_INT,
						0,
						1, //O flag indica que ainda hai datos.
						MPI_COMM_WORLD);
		}

		MPI_Ssend(subarray,
				 1,
				 MPI_INT,
				 0,
				 0, //O flag indica que xa non hai datos.
				 MPI_COMM_WORLD);
	}
	else
	{
		new_tree(&tree);
		indiceres = 0;
		//METEMOS NA ARBORE OS DATOS DE TODOS OS PROCESOS

		#ifdef DEBUG
			printf("Proc. %i envia dato num. %i de %i : %i\n",myrank,indiceroot,recvcount,subarray[indiceroot]);
		#endif

		insert_tree(&tree,subarray[0],0); //Introducese o dato do proc 0 (root)
		indiceroot = 1;

		//METEMOS EL PRIMER ELEMENTO DE CADA PROCESO
		for(proc=1;proc<numprocs;proc++)
		{
			MPI_Recv( 	&value,
						1,
					 	MPI_INT,
					 	proc,
					 	MPI_ANY_TAG,
					 	MPI_COMM_WORLD,
					 	&status);

			if (status.MPI_TAG)
				insert_tree(&tree,value,proc);
		}

		// EN CADA ITERACIÓN SACASE O VALOR MAIS ALTO E PIDESELLE
		// 	O PROCESADOR QUE TIÑA ESE VALOR OUTRO DATO.
		do
		{
			pop_smaller_tree(&tree,&value,&proc); // sacamos el elemento mas grande del arbol.
			data[indiceres++] = value;

			if(proc)
			{ //El elemento lo proporciona un proc. != root
				MPI_Recv( 	&value,
							1,
						 	MPI_INT,
						 	proc,
						 	MPI_ANY_TAG,
						 	MPI_COMM_WORLD,
						 	&status);

				if (status.MPI_TAG)
				{
					insert_tree(&tree,
								value,
								proc);
				}
			}
			else
			if(indiceroot < sendcounts[0])
			{
				#ifdef DEBUG
					printf("Proc. %i envia dato num. %i de %i : %i\n",myrank,indiceroot,recvcount,subarray[indiceroot]);
				#endif

				insert_tree(&tree,
							subarray[indiceroot++],
							0);
			}
		}while(no_empty_tree(tree));
	}

	if(!myrank){
	    mytime = MPI_Wtime() - mytime;  /*get time just after work section*/
	    mytime = mytime * MPI_Wtick();

		#ifdef CHECK
			printf("FINAL Check: %s\n", check_results(data,numberofitems) ? "Correct" : "Incorrect");
		#endif

		//Print sorted array.
		print_results(data,numberofitems);
		printf("numberofthreads = %i\n",omp_thread_count());
		printf("numberofprocs = %i\n",numprocs );
		printf ("numberofitems = %ld\n", numberofitems);
		printf ("Tiempo        = %f (segs) \n", mytime);
	}

	MPI_Finalize();
   return 0;
}