#include <stdlib.h>
#include <assert.h>


typedef struct node_t node;

struct node_t {
	int value;
    int proc;
    node *child[2];
};

typedef struct node_t* t_tree;


int
new_tree( t_tree *t){
	*t = NULL;
}

int
insert_tree(	t_tree *t, 
				int value,
				int proc)
{
	if(!*t){
		*t = malloc(sizeof(struct node_t));
    	(*t)->proc = proc;
		(*t)->value = value;
		(*t)->child[0] = NULL;
		(*t)->child[1] = NULL;
		return 0;
	}
	else
	if((*t)->value > value)
	{
		insert_tree((*t)->child,value, proc);
	}
	else
	{
		insert_tree((*t)->child+1,value,proc);
	}
}


int
pop_smaller_tree (t_tree *t,
				int *value,
				int *proc)
{
	t_tree aux;

	assert(no_empty_tree(*t));

	if (!*t)
	{
		return -1;
	}
	else
	if ((*t)->child[0] == NULL)
	{
		*value = (*t)->value;
		*proc = (*t)->proc;
		aux = *t;
		*t = (*t)->child[1];
		free(aux);
		return 0;
	}
	else
	{
		return pop_smaller_tree(	&((*t)->child[0]),
									value,
									proc);
	}

}

inline int
no_empty_tree(t_tree t)
{
	return t != NULL;
}