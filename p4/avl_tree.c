#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int max(int a, int b) { return a > b ? a : b; }

typedef struct node
{
	int val;
    short proc;
	struct node *left;
	struct node *right;
	struct node *parent;
	int height;
} node_t;

typedef node_t* t_tree;

int height(t_tree root)
{
    return root ? root->height : 0;
}

void adjust_height(t_tree root)
{
	root->height = 1 + max(height(root->left), height(root->right));
}

/* We can assume node->left is non-null due to how this is called */
t_tree rotate_right(t_tree root)
{
	/* Fix pointers */
	node_t *new_root = root->left;
	if (root->parent)
	{
		if (root->parent->left == root) root->parent->left = new_root;
		else root->parent->right = new_root;
	}
	new_root->parent = root->parent;
	root->parent = new_root;
	root->left = new_root->right;
    if (root->left) root->left->parent = root;
	new_root->right = root;

	/* Fix heights; root and new_root may be wrong. Do bottom-up */
	adjust_height(root);
	adjust_height(new_root);
	return new_root;
}

/* We can assume node->right is non-null due to how this is called */
t_tree rotate_left(t_tree root)
{
	/* Fix pointers */
	node_t *new_root = root->right;
	if (root->parent)
	{
		if (root->parent->right == root) root->parent->right = new_root;
		else root->parent->left = new_root;
	}
	new_root->parent = root->parent;
	root->parent = new_root;
	root->right = new_root->left;
    if (root->right) root->right->parent = root;
	new_root->left = root;

	/* Fix heights; root and new_root may be wrong */
	adjust_height(root);
	adjust_height(new_root);
	return new_root;
}

t_tree make_node(int val, int proc, t_tree parent)
{
	t_tree n = malloc(sizeof(node_t));
	n->val = val;
    n->proc = proc;
    n->parent = parent;
	n->height = 1;
	n->left = NULL;
	n->right = NULL;

	return n;
}

node_t *balance(node_t *root)
{
    if (height(root->left) - height(root->right) > 1)
    {
        if (height(root->left->left) > height(root->left->right))
        {
            root = rotate_right(root);
        }
        else
        {
            rotate_left(root->left);
            root = rotate_right(root);
        }
    }
    else if (height(root->right) - height(root->left) > 1)
    {
        if (height(root->right->right) > height(root->right->left))
        {
            root = rotate_left(root);
        }
        else
        {
            rotate_right(root->right);
            root = rotate_left(root);
        }
    }
    return root;
}

t_tree
insert_tree (t_tree root,
            int val,
            int proc)
{
    node_t *current = root;

    if(current == NULL)
        return make_node(val,proc,current);

    while (current->val != val)
    {
        if (val <= current->val)
        {
            if (current->left)
            {
                current = current->left;
            }
            else
            {
                current->left = make_node(val, proc, current);
                current = current->left;
            }
        }
        else if (val > current->val)
        {
            if (current->right)
            {
                current = current->right;
            }
            else
            {
                current->right = make_node(val, proc, current);
                current = current->right;
            }
        }
    }

    do
    {
        current  = current->parent;
        adjust_height(current);
        current = balance(current);
    } while (current->parent);

    return current;
}


void
pop_smaller_tree    (t_tree *t,
                    int *value,
                    int *proc)
{
    t_tree current;

    assert(*t);

    for(current = *t; current->left != NULL; current = current->left);

    *value = current->val;
    *proc = current->proc;

    if ( current->parent == NULL )
    {
        *t = current->right;
        if(current->right)
            current->right->parent = NULL;
        free(current);
        return;
    }

    current->parent->left = current->right;
    if(current->right)
        current->right->parent = current->parent;
    free(current);
}

t_tree
new_tree ()
{
    return NULL;
}

/*

int
main(int argc, char const *argv[])
{
    int v,p;
    t_tree t;
    t = new_tree();
    t = insert_tree(t,89,3);
    t = insert_tree(t,8,3);
    t = insert_tree(t,9,3);
    t = insert_tree(t,12,3);
    t = insert_tree(t,90,3);
    t = insert_tree(t,13,3);
    t = insert_tree(t,100,3);
    t = insert_tree(t,1,3);

    while(t){
        pop_smaller_tree(&t,&v,&p);
        printf("%i\n",v);
    }
}

*/