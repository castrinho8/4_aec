// Programa que calcula una norma para cada una de las     |
// filas de una matriz bidimensional representada mediante |
// un formato de almacenamiento comprimido.                |
#include <assert.h>	
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pvm3.h>

#ifndef DEBUG
#define DEBUG 0
#endif

#define SEND_TAG 42
#define INDEX(i,j) [(i)*ncols+j]


void
enviar_tamanho_local(	int * tids,
						int nproc,
						int nfilas,
						int ncols)
{
	int tam_local;

	if((nfilas % nproc)>1)
	{

		tam_local = (nfilas / nproc) + 1;

		//Crea y limpia un buffer de envio
		if( pvm_initsend( PvmDataDefault ) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_initsend \n");
			pvm_exit();
			exit(0);
		}

		//Empaqueta num_columnas
		if( pvm_pkint(&ncols,1, 1) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_pkint(ncols) \n");
			pvm_exit();
			exit(0);
		}

		//Empaqueta el tam_local
		if( pvm_pkint(&tam_local,1, 1) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_pkint(tam_local) \n");
			pvm_exit();
			exit(0);
		}

		//Envia la informacion empaquetada a los otros procesos
		if( pvm_mcast(tids, (nfilas % nproc) -1, SEND_TAG) ) {
			pvm_perror ("ERROR configure_parallel_program(): pvm_mcast \n");
			pvm_exit();
			exit(0);
		}

		tam_local = nfilas / nproc;

		//Crea y limpia un buffer de envio
		if( pvm_initsend( PvmDataDefault ) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_initsend \n");
			pvm_exit();
			exit(0);
		}

		//Empaqueta num_columnas
		if( pvm_pkint(&ncols,1, 1) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_pkint(ncols) \n");
			pvm_exit();
			exit(0);
		}

		//Empaqueta el tam_local
		if( pvm_pkint(&tam_local,1, 1) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_pkint(tam_local) \n");
			pvm_exit();
			exit(0);
		}

		if(nproc - (nfilas%nproc) > 1)
		{
			//Envia la informacion empaquetada a los otros procesos
			if( pvm_mcast(tids + (nfilas % nproc) - 1, nproc - (nfilas % nproc), SEND_TAG) ) {
				pvm_perror ("ERROR configure_parallel_program(): pvm_mcast \n");
				pvm_exit();
				exit(0);
			}
		}
		else
		{
			assert((nproc - (nfilas%nproc)) == 1);
			//Envia la informacion empaquetada a los otros procesos
			if( pvm_send(tids[(nfilas % nproc) - 1], SEND_TAG) < 0)
			{
				pvm_perror ("ERROR configure_parallel_program(): pvm_send \n");
				pvm_exit();
				exit(0);
			}
		}

	}
	else
	{

		tam_local = nfilas / nproc;

		//Crea y limpia un buffer de envio
		if( pvm_initsend( PvmDataDefault ) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_initsend \n");
			pvm_exit();
			exit(0);
		}

		//Empaqueta num_columnas
		if( pvm_pkint(&ncols,1, 1) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_pkint(ncols) \n");
			pvm_exit();
			exit(0);
		}

		//Empaqueta el tam_local
		if( pvm_pkint(&tam_local,1, 1) < 0 )
		{
			pvm_perror ("ERROR configure_parallel_program(): pvm_pkint(tam_local) \n");
			pvm_exit();
			exit(0);
		}


		//Envia la informacion empaquetada a los otros procesos
		if( pvm_mcast(tids, nproc - 1, SEND_TAG) ) {
			pvm_perror ("ERROR configure_parallel_program(): pvm_mcast \n");
			pvm_exit();
			exit(0);
		}

	}
}

void
recibir_tamanho_local(	int * nfilas,
					 	int * ncols)
{

	//Recibe numero de procesos, tidy array y tam_local
	if( pvm_recv(-1, SEND_TAG) < 0 ) {
		pvm_perror ("ERROR recibir_tamanho_local(): pvm_recv \n");
		pvm_exit();
		exit(0);
	}

	//Desempaqueta num_columnas
	if( pvm_upkint(ncols,1, 1) < 0 ){
		pvm_perror ("ERROR recibir_tamanho_local(): pvm_pkint(tam_local) \n");
		pvm_exit();
		exit(0);
	}

	//Desempaqueta el numero de filas subarray
	if( pvm_upkint(nfilas,1, 1) < 0 ){
		pvm_perror ("ERROR recibir_tamanho_local(): pvm_pkint(tam_local) \n");
		pvm_exit();
		exit(0);
	}

	if (DEBUG) printf("Proceso esclavo desempaqueta num_cols=%d nfilas=%d\n",*ncols,*nfilas);

}

void
enviar_sub_array(float * array, int nfilas, int ncols, int * tids, int nprocs)
{
	int index = 0, i, tam_local;

	index = ((nfilas / nprocs) + ((nfilas % nprocs) != 0)) * ncols;

	for(i=0;i<nprocs-1;i++)
	{

		//La segunda parte de la suma corresponde a 1 si el reparto no es igual y corresponde a los primeros procesadores.
		tam_local = (nfilas / nprocs + ((nfilas % nprocs) && ((i +1) < (nfilas % nprocs)))) * ncols;

		printf("Enviado a proc %i tamlocal=%i index=%i\n",i,tam_local,index);

		//Crea y limpia un buffer de envio
		if( pvm_initsend( PvmDataDefault ) < 0 ) {
			pvm_perror ("ERROR enviar_sub_array(): pvm_initsend \n");
			pvm_exit();
			exit(0);
		}

		//Empaqueta el sub_array
		if( pvm_pkfloat(array + index, tam_local, 1) < 0 ){
			pvm_perror ("ERROR enviar_sub_array(): pvm_pkint(tam_local) \n");
			pvm_exit();
			exit(0);
		}

		if( pvm_send(tids[i],SEND_TAG) < 0 ) {
			pvm_perror ("ERROR dowork(): pvm_send \n");
			pvm_exit();
			exit(0);
		}

		index += tam_local;
	}
}

void
recibir_sub_array(int id_padre, int nfilas, int ncols,float ** sub_array)
{

	//RESERVAMOS MEMORIA PARA SUBARRAY
	*sub_array = malloc(sizeof(float)* nfilas * ncols );
	assert(*sub_array);

	//Recibe numero de procesos, tidy array y tam_local
	if( pvm_recv(id_padre, SEND_TAG) < 0 ) {
		pvm_perror ("ERROR recibir_sub_array(): pvm_recv \n");
		pvm_exit();
		exit(0);
	}

	//Desempaqueta el id del padre
	if( pvm_upkfloat(*sub_array, nfilas * ncols, 1) < 0 ){
		pvm_perror ("ERROR recibir_sub_array(): pvm_pkint(tids) \n");
		pvm_exit();
		exit(0);
	}
}

void
recibir_resultados(	int * tids,
				 	int nprocs,
				 	float ** norma_matriz,
				 	int nfilas,
				 	int ncols)
{
	int index = 0, i, tam_local;

	*norma_matriz = malloc(sizeof(float) * nfilas);

	index = (nfilas / nprocs) + ((nfilas % nprocs) != 0);

	for(i=0;i<nprocs-1;i++){

		tam_local = (nfilas / nprocs) + ((nfilas % nprocs) && ((i +1) < (nfilas % nprocs)));

		//Recibe numero de procesos, tidy array y tam_local
		if( pvm_recv(tids[i], SEND_TAG) < 0 ) {
			pvm_perror ("ERROR recibir_resultados(): pvm_recv \n");
			pvm_exit();
			exit(0);
		}

		//DesEmpaqueta el sub_array
		if( pvm_upkfloat(*norma_matriz + index, tam_local, 1) < 0 ){
			pvm_perror ("ERROR recibir_resultados(): pvm_pkint(tam_local) \n");
			pvm_exit();
			exit(0);
		}

		index += tam_local;
		printf("Recibidos resultados de proc. %i\n",i);
	}
}

void
enviar_resultados(	int id_padre,
				 	float * norma_local,
				 	int nfilas)
{
	//Crea y limpia un buffer de envio
	if( pvm_initsend( PvmDataDefault ) < 0 ) {
		pvm_perror ("ERROR enviar_resultados(): pvm_initsend \n");
		pvm_exit();
		exit(0);
	}

	//Empaqueta el sub_array
	if( pvm_pkfloat(norma_local, nfilas, 1) < 0 ){
		pvm_perror ("ERROR enviar_resultados(): pvm_pkint(tam_local) \n");
		pvm_exit();
		exit(0);
	}

	if( pvm_send(id_padre,SEND_TAG) < 0 ) {
		pvm_perror ("ERROR enviar_resultados(): pvm_send \n");
		pvm_exit();
		exit(0);
	}
}



int
do_work(float * sub_array, int nfilas, int ncols, float ** norma_local)
{
	int i,j;
	float normafila;

	//RESERVAMOS MEMORIA PARA RESULTADOS
	*norma_local = malloc(sizeof(float)*nfilas);
	assert(*norma_local);

	for (i=0; i<nfilas; i++)
	{
		normafila = 0;
		for (j=0; j<ncols; j++)
	    	normafila += (sub_array INDEX(i,j) * sub_array INDEX(i,j));
		(*norma_local)[i] = sqrt(normafila);
	}

}


void
generar_matriz(	float ** matriz,
				int nfilas,
				int ncols)
{
	int j,k;

	//Reservase espacio para matriz a calcular.
	*matriz = malloc(sizeof(float)*nfilas*ncols);
	assert(*matriz);

	//Generase a matriz de forma aleatoria.
	for (j=0; j<nfilas; j++)
		for (k=0; k<ncols; k++)
			(*matriz) INDEX(j,k) = (float) ( (float) random() / (float) RAND_MAX);

}

int main( int argc, char *argv[]) {

	//Variables practica
	float * pvalores, *norma_matriz, *norma_local_root;
	int i,j;
	int nfilas, ncols, nfilas_root;
	struct timeval t0, t1, t;
   	int nproc;                 /* number of processes */
   	int *tids;                 /* array of 'nproc' process ids */
	int id_padre, mytid;

    /* enroll in pvm */
    mytid = pvm_mytid();

    if (DEBUG) printf("Creado proceso id=%i!!\n",mytid);

    if( (id_padre = pvm_parent()) == PvmNoParent ) //PAI
    {
    	if (argc != 4)
    	{
			printf ("Uso: norma nfilas ncols nprocesos\n");
			exit(0);
		}

		nfilas = atoi(argv[1]);
		ncols  = atoi(argv[2]);
		nproc  = atoi(argv[3]);

		//Se reserva espacio para el array de procesos
		tids =  malloc((nproc -1) * sizeof(int));
		assert(tids);

		printf("Proceso ROOT: id=%i. Se generan %i hijos.\n",mytid,nproc-1);

		//Crea copias del fichero ejecutable en la maquina virtual
		if(pvm_spawn(argv[0], (char**)0, 0, "",  nproc-1, tids) != nproc-1 ) {
			pvm_perror ("ERROR configure_parallel_program(): Not enough processes created \n");
			pvm_exit();
			exit(0);
		}

		if (DEBUG) printf("Proceso ROOT: Generando Matriz.\n");

		generar_matriz(&pvalores,nfilas,ncols);

		assert(gettimeofday (&t0, NULL) == 0);

		if (DEBUG) printf("Proceso ROOT: Enviando tam. local.\n");

		enviar_tamanho_local(tids, nproc, nfilas, ncols);

		if (DEBUG) printf("Proceso ROOT: Enviando subarray.\n");

		enviar_sub_array(pvalores, nfilas, ncols, tids, nproc);

		if (DEBUG) printf("Proceso ROOT: Realizando calculos. nfilas=%i\n",nfilas/nproc + ((nfilas % nproc) != 0));

		nfilas_root = nfilas/nproc + ((nfilas % nproc) != 0);

		do_work(pvalores, nfilas_root, ncols, &norma_local_root);

		if (DEBUG) printf("Proceso ROOT: Recibiendo resultados.\n");

		recibir_resultados(tids, nproc, &norma_matriz, nfilas, ncols);

		for (i = 0; i < nfilas_root; ++i)
		{
			norma_matriz[i] = norma_local_root[i];
		}

		assert(gettimeofday (&t1, NULL) == 0);
		timersub(&t1, &t0, &t);

		for(i=0;i<5;i++) printf("Norma[%d] = %.2f \n",i,norma_matriz[i]);
	    for(i=nfilas-5;i<nfilas;i++) printf("Norma[%d] = %.2f \n",i,norma_matriz[i]);

		printf ("nfilas      = %i\n", nfilas);
		printf ("ncols       = %i\n", ncols);
		printf ("Tiempo      = %ld:%ld(seg:mseg)\n", t.tv_sec, t.tv_usec/1000);

		free(pvalores);
		free(norma_matriz);

		if(pvm_exit() < 0)
		{
			pvm_perror ("ERROR main(): pvm_exit \n");
   		}
	}
	else //FILLOS
	{

		printf("Proceso SLAVE: id=%i, padre=%i\n",mytid,id_padre);

		if (DEBUG) printf("Proceso SLAVE: Recibiendo tam. local.\n");
		recibir_tamanho_local(&nfilas, &ncols);

		if (DEBUG) printf("Proceso SLAVE: Recibiendo subarray.\n");

		recibir_sub_array(id_padre, nfilas, ncols,&pvalores);

		if (DEBUG) printf("Proceso SLAVE: Realizando calculos. nfilas=%i\n",nfilas);

		do_work(pvalores, nfilas, ncols, &norma_matriz);

		if (DEBUG) printf("Proceso SLAVE: Enviando resultados.\n");

		enviar_resultados(id_padre, norma_matriz, nfilas);

		free(pvalores);
		free(norma_matriz);

		pvm_exit();

	}

	return 0;
}

