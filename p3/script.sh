#!/bin/bash

repeticiones=3
echo "------NOMBRE DEL EQUIPO----"
uname -a
echo "\n---------INFO CPU----------"
cat /proc/cpuinfo
echo "\n---------INFO MEM----------"
cat /proc/meminfo

for app in "pvm1" "pvm2"
do
	echo -e "\n\n......................................"
	echo -e "-------------------$app-----------------"
	echo -e "......................................\n"

	for proc in `seq 2 24`
	do
		echo -e  "\n\nAHORA CON $proc PROCESADORES"
		for i in `seq $repeticiones`
		do
			~/pvm3/bin/LINUX/$app 15000 15000 $proc  |  grep "Tiempo"
		done
	done
done
